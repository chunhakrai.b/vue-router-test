import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path';
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);

const __dirname = path.dirname(__filename);

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [vue()],
    base: '/',
    build: {
        assetsDir: 'assets'
    },
    publicPath: "/",
    server: {
        host: '0.0.0.0'
    },
    resolve: {
        alias: {
            '@themeConfig': path.resolve(__dirname, 'theme.config.js'),
        }
    },
})
