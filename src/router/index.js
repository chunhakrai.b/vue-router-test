import {createRouter, createWebHistory} from 'vue-router'

// import Homepage from '../views/index.vue'
import HelloWorld from '../components/HelloWorld.vue'

const routes = [
    // {path: '/', name: 'Home', component: Homepage},
    {path: '/test', name: 'TEST-PAGE', component: import('../components/test-page.vue')},
    {path: '/hello', name: 'TEST-HELLO', component: HelloWorld},
    // {
    //     path: '/order',
    //     name: 'Order',
    //     component: import(/* webpackChunkName: "order-page" */ '../views/order/index.vue'),
    //     children: [
    //         {
    //             path: '/order/new',
    //             component: import(/* webpackChunkName: "order-new-order-page" */ '../views/order/new-order.vue')
    //         },
    //         {
    //             path: '/order/lists',
    //             component: import(/* webpackChunkName: "order-list-order-page" */ '../views/order/list-order.vue')
    //         }
    //     ]
    // },
    // {
    //     path: '/kitchen',
    //     name: 'Kitchen',
    //     component: import(/* webpackChunkName: "kitchen-page" */ '../views/kitchen/index.vue')
    // },
    // {
    //     path: '/table',
    //     name: 'Table',
    //     component: import(/* webpackChunkName: "table-page" */ '../views/table/index.vue')
    // },
    // {
    //     path: '/table/check-bill',
    //     name: 'Check Bill',
    //     component: import(/* webpackChunkName: "table-page" */ '../views/table/check-bill.vue')
    // },
    // {
    //     path: '/menu',
    //     name: 'Menu',
    //     component: import(/* webpackChunkName: "menu-page" */ '../views/menu/index.vue')
    // },
    // {
    //     path: '/:pathMatch(.*)',
    //     name: 'catchAll',
    //     component: import('../components/not-found')
    // }
]

const router = createRouter({
    // mode: 'history',
    history: createWebHistory(),
    // linkExactActiveClass: 'active',
    linkActiveClass: "active",
    routes,
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return {left: 0, top: 0}
        }
    },
})

router.beforeEach((to, from, next) => {
    document.title = to?.name ? to.name : 'Restaurant System'
    console.log(to)
    next(true)
})
export default router
